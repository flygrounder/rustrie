class Node
{
    constructor(letter, alphabetLength)
    {
        var INF = 1e9 + 7;
        this.size = 0;
        this.minPage = INF;
        this.maxPage = -INF;
        this.letter = letter;
        this.edges = [];
        this.definition = null;
        for (let i = 0; i < alphabetLength; ++i)
            this.edges.push(null);
    }
}


class Trie
{
    constructor(alphabetLength, initialLetter)
    {
        this.alphabetLength = alphabetLength;
        this.initialLetterCode = initialLetter.charCodeAt(0);
        this.head = new Node('', alphabetLength);
    }

    addWord(word, definition, page, pos=0, currentNode=null)
    {
        if (pos == 0)
            currentNode = this.head;
        if (pos == word.length)
        {
            currentNode.definition = definition;
            currentNode.minPage = Math.min(page, currentNode.minPage);
            currentNode.maxPage = Math.max(page, currentNode.maxPage);
        }
        else
        {
            let requiredPosition = word.charCodeAt(pos) - this.initialLetterCode;
            if (currentNode.edges[requiredPosition] == null)
            {
                currentNode.edges[requiredPosition] = new Node(word[pos], this.alphabetLength);
            }
            this.addWord(word, definition, page, pos + 1, currentNode.edges[requiredPosition]);
            currentNode.minPage = Math.min(currentNode.minPage, currentNode.edges[requiredPosition].minPage);
            currentNode.maxPage = Math.max(currentNode.maxPage, currentNode.edges[requiredPosition].maxPage);
        }
        ++currentNode.size;
    }

    findNode(prefix)
    {
        let currentNode = this.head;
        for (let i = 0; i < prefix.length; ++i)
        {
            let requiredPosition = prefix[i].charCodeAt(0) - this.initialLetterCode;
            if (currentNode.edges[requiredPosition] == null)
            {
                return false;
            }
            currentNode = currentNode.edges[requiredPosition];
        }
        return currentNode;
    }
}